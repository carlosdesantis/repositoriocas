var topics      = require('./topics.json');
var news        = require('./news.json');
var statistics  = require('./statistics.json');
var studies     = require('./studies.json');
var users       = require('./users.json');
var contact_s   = require('./contact-submission.json');
var faqs        = require('./faqs.json');
var publics     = require('./publications.json');
var search      = require('./search-result.json');
var faqsearch   = require('./faqs-search.json');
var content     = require('./content.json');
var comments    = require('./comments.json');
var likes       = require('./likes.json');
var login       = require('./login.json');
var upload      = require('./upload_record.json');
var pending     = require('./pending_tasks.json');

module.exports = function() {
  return {
    topics:     topics,
    news:       news,
    statistics: statistics,
    studies:    studies,
    users:      users,
    contact_s:  contact_s,
    faqs:       faqs,
    publics:    publics,
    search:     search,
    faqsearch:  faqsearch,
    content:    content,
    comments:   comments,
    likes:      likes,
    login:      login,
    upload:     upload,
    pending:    pending
  }
}
