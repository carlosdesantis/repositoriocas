import { BrowserModule }              from '@angular/platform-browser';
import { NgModule }                   from '@angular/core';
import { HttpClientModule, HttpClientJsonpModule, HTTP_INTERCEPTORS }           from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule }    from '@angular/platform-browser/animations';
import { TubosDerechaPipe }           from './pipes/tubos-derecha.pipe';
import { SlashIzquierdaPipe }         from './pipes/slash-izquierda.pipe';
import { SlashDerechaPipe }           from '../app/pipes/slash-derecha.pipe';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
// Import your library
import { SlickCarouselModule }        from "ngx-slick-carousel";

import { AppRoutingModule }           from './app-routing.module';
import { AppComponent }               from './app.component';

import { TopComponent }               from './navbar/top/top.component';
import { BottomComponent }            from './navbar/bottom/bottom.component';
import { HeaderComponent }            from './navbar/header/header.component';
import { FooterComponent }            from './navbar/footer/footer.component';

import { HomeComponent }              from './templates/home/home.component';
import { CoverFirstLevelComponent }   from './templates/cover-first-level/cover-first-level.component';
import { CoverSecondLevelComponent }  from './templates/cover-second-level/cover-second-level.component';
import { BibliometricsComponent }     from './templates/bibliometrics/bibliometrics.component';
import { ContactComponent }           from './templates/contact/contact.component';
import { CoverContentComponent }      from './templates/cover-content/cover-content.component';
import { CoverNoveltyComponent }      from './templates/cover-novelty/cover-novelty.component';
import { FaqComponent }               from './templates/faq/faq.component';
import { PageContentComponent }       from './templates/page-content/page-content.component';
import { ProfileResearcherComponent } from './templates/profile-researcher/profile-researcher.component';
import { ProfileTeamComponent }       from './templates/profile-team/profile-team.component';
import { SearchAdvancedComponent }    from './templates/search-advanced/search-advanced.component';
import { SingleNoveltyComponent }     from './templates/single-novelty/single-novelty.component';
import { SingleRecordComponent }      from './templates/single-record/single-record.component';
import { UploadRecordComponent }      from './templates/upload-record/upload-record.component';

import { CardNormalComponent }        from './partials/cards/card-normal/card-normal.component';
import { CardHorizontalComponent }    from './partials/cards/card-horizontal/card-horizontal.component';
import { CardTopicComponent }         from './partials/cards/card-topic/card-topic.component';
import { CardBibliometricsComponent } from './partials/cards/card-bibliometrics/card-bibliometrics.component';
import { CardPublicationsComponent }  from './partials/cards/card-publications/card-publications.component';

import { TitleComponent }             from './partials/commons/title/title.component';
import { BreadcrumbsComponent }       from './partials/commons/breadcrumbs/breadcrumbs.component';
import { SearchFormComponent }        from './partials/commons/search-form/search-form.component';

import { SliderComponent }            from './partials/media/slider/slider.component';
import { DescargaGaComponent } from './templates/descarga-ga/descarga-ga.component';



import { RequestInterceptorService } from './request-interceptor.service';
import { AuthService } from './auth.service';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TopComponent,
    BottomComponent,
    HeaderComponent,
    FooterComponent,
    CardTopicComponent,
    TitleComponent,
    SliderComponent,
    CardHorizontalComponent,
    CardNormalComponent,
    CoverFirstLevelComponent,
    CoverSecondLevelComponent,
    BibliometricsComponent,
    ContactComponent,
    CoverContentComponent,
    CoverNoveltyComponent,
    FaqComponent,
    PageContentComponent,
    ProfileResearcherComponent,
    ProfileTeamComponent,
    SearchAdvancedComponent,
    SingleNoveltyComponent,
    SingleRecordComponent,
    UploadRecordComponent,
    CardBibliometricsComponent,
    BreadcrumbsComponent,
    CardPublicationsComponent,
    SearchFormComponent,
    DescargaGaComponent,
    TubosDerechaPipe,
    SlashIzquierdaPipe,
    SlashDerechaPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientJsonpModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    // Specify your library as an import
    SlickCarouselModule
  ],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
