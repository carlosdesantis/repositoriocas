import { Component, OnInit, DoCheck } from '@angular/core';
import { QueriesService }    from '../../services/queries.service';
import { Router, ActivatedRoute, Params }     from '@angular/router';
import json                  from './cover-second-level.json';
import busqueda_tesis        from './05-busqueda-tesis.json';
import compartir             from './02-compartir-investigacion.json';

@Component({
  selector: 'app-cover-second-level',
  templateUrl: './cover-second-level.component.html',
  styleUrls: ['./cover-second-level.component.css']
})
export class CoverSecondLevelComponent implements OnInit, DoCheck {

  data:           any = [];
  json:           any = json;
  busqueda_tesis: any = busqueda_tesis;
  compartir:      any = compartir;

  public pagina:  string;

  constructor(
    private queriesService: QueriesService,
    private activatedRoute: ActivatedRoute, 
    private router: Router
    ) { }

  ngOnInit() {
    
    this.queriesService.queryGet('http://localhost:3000/topics').then((data) => {this.data['topics'] = data; console.log(this.data['topics'])});
    this.queriesService.queryGet('http://localhost:3000/publics').then((data) => {this.data['publics'] = data});

    this.activatedRoute.params.subscribe(params =>{
      this.pagina = params['pag'];
      // JQuery ir arriba
      $('body, html').animate({
        scrollTop: '0px'
      }, 300);
    });
  }
  ngDoCheck(){
    this.json = json;
    this.paginas();

  }

  paginas(){
      // MENU TESIS
      if(this.pagina == "tesis-doctorado-magister"){
        this.json = this.json[this.pagina ];
        
      }
      if(this.pagina == "trabajos-graduacion"){
        this.json = this.json[this.pagina ];
      }
      // MENU PUBLICACIONES
      if(this.pagina == "articulos-academicos"){
        this.json = this.json[this.pagina ];
      }
      if(this.pagina == "capitulos-libros"){
        this.json = this.json[this.pagina ];
      }
      if(this.pagina == "libros"){
        this.json = this.json[this.pagina ];
      }
      if(this.pagina == "documentos-trabajo"){
        this.json = this.json[this.pagina ];
      }
      if(this.pagina == "presentaciones-congresos"){
        this.json = this.json[this.pagina ];
      }
      if(this.pagina == "obras-artisticas"){
        this.json = this.json[this.pagina ];
      }
      if(this.pagina == "otros"){
        this.json = this.json[this.pagina ];
      }
      
  }
  
}
