import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params }     from '@angular/router';
import { Title, Meta }  from '@angular/platform-browser';
import {Location} from '@angular/common';

@Component({
  selector: 'app-descarga-ga',
  templateUrl: './descarga-ga.component.html',
  styleUrls: ['./descarga-ga.component.css']
})
export class DescargaGaComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute, 
    private meta: Meta,
    private title: Title,
    private _location: Location
  ) { }

  ngOnInit() {

    this.activatedRoute.params.subscribe(params =>{
      this.title.setTitle( params['titulo'] );
    });
    this._location.back(); 
    
  }

}
