import { Component, OnInit }  from '@angular/core';
import { QueriesService }     from '../../services/queries.service';
import json                   from './contact.json';
import Faqs                   from '../../../assets/json/preguntas-frecuentes.json';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  data: any = [];
  main: any = {};
  response: any = {
    'show': false,
    'data': ''
  }
  name;
  email;
  message;

  constructor(private queriesService: QueriesService) { }

  ngOnInit() {
    this.main = json;
    this.data['faqs'] = Faqs['preguntas-frecuente'];
    //this.queriesService.queryGet('http://localhost:3000/faqs').then( (data) => { this.data['faqs'] = data; console.log(this.data['faqs']) });

    // JQuery ir arriba
    $('body, html').animate({
      scrollTop: '0px'
    }, 300);
    
  }

  resetForm() {
    this.response.show = false;
    this.response.text = '';
  }

  submitContact(form, $event) {
    $event.preventDefault();

    if(form.form.value)
      this.response.show = true;
      //this.response.text = 'Su mensaje ha sido enviado con éxito';

      /* this.queriesService.queryPost('http://localhost:3000/contact_s', form.form.value).then( */
      this.queriesService.queryGet('http://interfaz-rep-desa-lab.dca.uc.cl/mail-contacto/envio.php?nombreApellido='+this.name+'&mail='+this.email+'&consulta='+this.message).then(
      (data) => {
        if(data['respuesta'] == true){
          this.response.show = true;
          this.response.text = 'Su mensaje ha sido enviado con éxito';
        }else{
          console.log(data);
          this.response.show = true;
          this.response.text = 'Ha ocurrido un problema. Por favor, intente más tarde';
        }
      },
      (error) => {
        console.log(error);
        this.response.show = true;
        this.response.text = 'Ha ocurrido un problema. Por favor, intente más tarde';
      }
    );
  }
}
