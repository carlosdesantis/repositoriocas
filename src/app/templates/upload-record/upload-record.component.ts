import { Component, OnInit, ChangeDetectionStrategy, Input }  from '@angular/core';
import { FormGroup, FormBuilder, Validators }                 from '@angular/forms';
import { QueriesService }    from '../../services/queries.service';
import { Router, ActivatedRoute, Params }     from '@angular/router';
import json                  from './upload-record.json';
import compartir             from './02-compartir-investigacion.json';
import { animate, state, style, transition, trigger }         from '@angular/animations';

@Component({
  selector: 'app-upload-record',
  templateUrl: './upload-record.component.html',
  styleUrls: ['./upload-record.component.css'],

  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('slide', [
      state('t1', style({ transform: 'translateX(0)' })),
      state('t2', style({ transform: 'translateX(-25%)' })),
      state('t3', style({ transform: 'translateX(-50%)' })),
      state('t4', style({ transform: 'translateX(-75%)' })),
      transition('* => *', animate(300))
    ])
  ]
})
export class UploadRecordComponent implements OnInit {

  data: any = [];
  json: any = json;
  compartir: any = compartir;

  isLeftVisible = true;
  activePane: any = 't1';

  uploadForm: FormGroup;

  pagina;

  constructor(
    private queriesService: QueriesService, 
    private activatedRoute: ActivatedRoute, 
    private router: Router,
    private formBuilder: FormBuilder) {
    this.createForm();
  }

  ngOnInit() {
    this.paginas();

    this.activatedRoute.params.subscribe(params =>{
      this.pagina = params['pag'];
      
      // JQuery ir arriba
      $('body, html').animate({
        scrollTop: '0px'
      }, 300);

    }); 
  }

  paginas(){
      // MENU TESIS
      if(this.pagina == "subir-tesis-trabajos"){
        this.json = this.json;
      }
      // MENU PUBLICACIONES
      if(this.pagina == "subir-publicacion"){
        this.json = this.json;
      }
  }

  saveRecord(form, $event) {
    $event.preventDefault();
    if(form.form.value) {
      this.queriesService.queryPost('http://localhost:3000/upload', form.form.value).then(
        (data) => { console.log(data) },
        (error) => {}
      );
    }
  }

  createForm() {
    this.uploadForm = this.formBuilder.group({
      input_tematica: ['', Validators.required],
      input_titulo_investigacion: ['', Validators.required],
      input_importancia: [''],
      input_otros_titulos: [''],
      input_resumen: [''],
      input_fecha_publicacion: ['', Validators.required],
      input_extension_obra: [''],
      input_etiquetas: [''],
      input_serie: [''],
      input_numero_informe: [''],
      input_idioma: ['', Validators.required],
      input_tipo_material: ['', Validators.required],
      input_seleccionar_archivo: ['', Validators.required],
      input_descripcion_archivo: [''],
      input_tipo_acceso: [''],
      input_fecha_embargo: [''],
      input_autor_principal_name: ['', Validators.required],
      input_autor_principal_lastname: ['', Validators.required],
      input_autor_secundario_name: [''],
      input_autor_secundario_lastname: [''],
      input_editor_name: [''],
      input_editor_lastname: [''],
      input_profesor_name: [''],
      input_profesor_lastname: [''],
      input_codigo_autor: [''],

      input_investigaciones_relacionadas: [''],
      input_imagen_destacada: [''],
      input_galeria_imagenes: [''],
      input_video_asociado: [''],
      input_texto_destacado: ['', Validators.required],
      input_como_citar: [''],
      input_conceder_licencia: ['', Validators.required],
    });
  }
}
