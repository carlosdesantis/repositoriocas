import { Component, OnInit, DoCheck } from '@angular/core';
import { QueriesService }    from '../../services/queries.service';
import { Router, ActivatedRoute, Params }     from '@angular/router';
import json                  from './cover-first-level.json';
import busqueda_tesis        from './05-busqueda-tesis.json';
import compartir             from './02-compartir-investigacion.json';

@Component({
  selector: 'app-cover-first-level',
  templateUrl: './cover-first-level.component.html',
  styleUrls: ['./cover-first-level.component.css']
})
export class CoverFirstLevelComponent implements OnInit, DoCheck {

  data:           any = [];
  main:           any = json;
  busqueda_tesis: any = busqueda_tesis;
  compartir:      any = compartir;

  pagina;

  constructor(
    private queriesService: QueriesService,
    private activatedRoute: ActivatedRoute, 
    private router: Router
    ) { }

  ngOnInit() {
    this.queriesService.queryGet('http://localhost:3000/topics').then((data) => {this.data['topics'] = data});
    this.queriesService.queryGet('http://localhost:3000/publics').then((data) => {this.data['publics'] = data});

    this.activatedRoute.params.subscribe(params =>{
      this.pagina = params['pag'];
    });
  }
  ngDoCheck(){
    this.main = json;
    this.paginas();
    
  }

  paginas(){
      if(this.pagina == "tesis"){
        this.main = this.main['tesis'];
      }
      if(this.pagina == "publicaciones"){
        this.main = this.main['publicaciones'];
      }
      if(this.pagina == "enlaces-recursos"){
        this.main = this.main['enlaces-recursos'];
      }
      if(this.pagina == "que-es-repositorio"){
        this.main = this.main[this.pagina ];
      }
  }


}
