import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { QueriesService }                         from '../../services/queries.service';
import json                                       from './single-novelty.json';

@Component({
  selector: 'app-single-novelty',
  templateUrl: './single-novelty.component.html',
  styleUrls: ['./single-novelty.component.css']
})
export class SingleNoveltyComponent implements OnInit {

  data: any = [];
  json: any = json;
  response: any = {
    'show': false,
    'data': ''
  };

  novelty_id: any = "0000";
  liked: any = false;

  constructor(private queriesService: QueriesService, private _sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.queriesService.queryGet('http://localhost:3000/news?id=' + this.novelty_id).then((data) => {this.data['news'] = data[0]});
    this.queriesService.queryGet('http://localhost:3000/publics').then((data) => {this.data['publics'] = data});
    this.queriesService.queryGet('http://localhost:3000/comments').then((data) => {this.data['comments'] = data});

    this.likeNovelty();
  }

  likeNovelty() {
    this.queriesService.queryGet('http://localhost:3000/likes').then((data) => {
      this.liked = data['liked'];
      this.queriesService.queryPost('http://localhost:3000/likes', {liked: !data['liked']});
    });
  }

  resetForm() {
    this.response.show = false;
    this.response.text = '';
  }

  submitComment(form, $event) {
    $event.preventDefault();
    if(form.form.value) {
      let date = new Date();
      let full_date = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes();
      let comment = {
        user: "Napoleón Gómez", //Reemplazar por el usuario actual que haya iniciado sesión
        date: full_date,
        text: form.form.value['comment_textarea']
      }
      this.queriesService.queryPost('http://localhost:3000/comments', comment).then(
        (data) => {
          this.queriesService.queryGet('http://localhost:3000/comments').then((data) => {this.data['comments'] = data});
          this.response.show = true;
          this.response.text = 'Su comentario ha sido publicado con éxito';
        },
        (error) => {
          this.response.show = true;
          this.response.text = 'Ha ocurrido un problema. Por favor, intente más tarde';
        }
      );
    }
  }

  getBackgroundImage(image) {
    return this._sanitizer.bypassSecurityTrustStyle(`linear-gradient(rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(${image})`);
  }
}
