import { Component, OnInit, DoCheck, ɵConsole }   from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { Router, ActivatedRoute, Params }         from '@angular/router';
import * as L                                     from 'leaflet/dist/leaflet.js';
import { QueriesService }                         from '../../services/queries.service';

import PoliticasAccesoAbierto                     from '../../../assets/json/page-content/politicas-de-acceso-abierto.json';
import QuienesSomos                               from '../../../assets/json/page-content/quienes-somos.json';
import PoliticasPrivacidad                        from '../../../assets/json/page-content/politicas-de-privacidad.json';
import CondicionesUso                             from '../../../assets/json/page-content/condiciones-de-uso.json';
import MapaSitio                                  from '../../../assets/json/page-content/mapa-del-sitio.json';

import SubidosRecientemente                       from '../../../assets/json/page-content/subidos-recientemente.json';
import MasDescargados                             from '../../../assets/json/page-content/mas-descargados.json';
import Destacados                                 from '../../../assets/json/page-content/destacados.json';
import InvestigacionMes                           from '../../../assets/json/page-content/investigacion-del-mes.json';

@Component({
  selector: 'app-page-content',
  templateUrl: './page-content.component.html',
  styleUrls: ['./page-content.component.css']
})
export class PageContentComponent implements OnInit, DoCheck {

  data: any = [];
  pagina;

  constructor(
    private _sanitizer: DomSanitizer, 
    private queriesService: QueriesService,
    private activatedRoute: ActivatedRoute, 
    private router: Router
    ) { }

  ngOnInit() {
    this.queriesService.queryGet('http://localhost:3000/content').then((data) => {
      this.data['content'] = data;

      setTimeout(() => {
        this.data['content']['horizon_contents'].forEach(function(content, index){
          if(content['type'] == 'map') {
            var mymap = L.map('map_'+index).setView([content['content']['lat'], content['content']['lon']], 18);
            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
              attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
              maxZoom: 18,
              id: 'mapbox.streets',
              accessToken: 'pk.eyJ1IjoiY2VubGFuIiwiYSI6ImNrMWIycTQwYTBlNXAzaG52bmdjM3VlMDcifQ.z7g583AnswTKIi2ESOnoNQ'
            }).addTo(mymap);
          }
        });
      }, 50);
    });

    this.activatedRoute.params.subscribe(params =>{
      this.pagina = params['pag'];
      // JQuery ir arriba
      $('body, html').animate({
        scrollTop: '0px'
      }, 300);
    }); 
  }
  ngDoCheck(){
    this.paginas();
  }

  paginas(){
      // MENU NOVEDADES
      if(this.pagina == "subidos-recientemente"){
        this.data['content'] = SubidosRecientemente;
      }
      if(this.pagina == "mas-descargados"){
        this.data['content'] = MasDescargados;
      }
      if(this.pagina == "destacados"){
        this.data['content'] = Destacados;
      }
      if(this.pagina == "investigacion-del-mes"){
        this.data['content'] = InvestigacionMes;
      }
      // MENU QUE ES REPOSITORIO
      if(this.pagina == "politicas-de-acceso-abierto"){
        this.data['content'] = PoliticasAccesoAbierto;
      }
      if(this.pagina == "quienes-somos"){
        this.data['content'] = QuienesSomos;
      }
      if(this.pagina == "politicas-de-privacidad"){
        this.data['content'] = PoliticasPrivacidad;
      }
      if(this.pagina == "condiciones-de-uso"){
        this.data['content'] = CondicionesUso;
      }
      if(this.pagina == "mapa-del-sitio"){
        this.data['content'] = MapaSitio;
      }
  }

  getBackgroundImage(image) {
    return this._sanitizer.bypassSecurityTrustStyle(`linear-gradient(rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(${image})`);
  }
}
