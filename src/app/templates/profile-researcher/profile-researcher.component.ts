import { Component, OnInit } from '@angular/core';
import json                  from './profile-researcher.json';
import { QueriesService }    from '../../services/queries.service';
@Component({
  selector: 'app-profile-researcher',
  templateUrl: './profile-researcher.component.html',
  styleUrls: ['./profile-researcher.component.css']
})
export class ProfileResearcherComponent implements OnInit {

  data: any = [];
  json: any = json;

  constructor(private queriesService: QueriesService) { }

  ngOnInit() {
    this.queriesService.queryGet('http://localhost:3000/users?id=00000001').then((data) => {this.data['user'] = data[0]});
    this.queriesService.queryGet('http://localhost:3000/topics').then((data) => {this.data['topics'] = data;});
    this.queriesService.queryGet('http://localhost:3000/publics').then((data) => {
      this.data['publics'] = data;
      console.log(this.data['publics']);
    });
  }

  printProfile(dataPrint) {
    let printContents = dataPrint.innerHTML;
    let originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
  }

}
