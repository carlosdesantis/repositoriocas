import { Component, OnInit }                      from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { QueriesService }                         from '../../services/queries.service';
import json                                       from './cover-novelty.json';

@Component({
  selector: 'app-cover-novelty',
  templateUrl: './cover-novelty.component.html',
  styleUrls: ['./cover-novelty.component.css']
})
export class CoverNoveltyComponent implements OnInit {

  data: any = [];
  json: any = json;

  constructor(private queriesService: QueriesService, private _sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.queriesService.queryGet('http://localhost:3000/news').then((data) => {this.data['news'] = data});
  }

  getBackgroundImage(image) {
    return this._sanitizer.bypassSecurityTrustStyle(`linear-gradient(rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(${image})`);
  }
}
