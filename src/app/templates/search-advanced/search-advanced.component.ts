import { Component, OnInit }  from '@angular/core';
import { ActivatedRoute, Params }     from '@angular/router';
import { FormGroup, FormBuilder, Validators }   from '@angular/forms';
import { Router }                   from '@angular/router';

import json                  from './search-advanced.json';
import { QueriesService }     from '../../services/queries.service';
import { global } from '../../services/global';

@Component({
  selector: 'app-search-advanced',
  templateUrl: './search-advanced.component.html',
  styleUrls: ['./search-advanced.component.css']
})
export class SearchAdvancedComponent implements OnInit {

  json: any = json;
  data: any = [];

  public urlPhp;
  public jsonFiltro: any ="";
  public objFiltro: any ="";
  public name_search_by: any = "";
  public name_contains: any = "";
  public name_term: any = "";
  public name_search_by2;
  public name_contains2;
  public name_term2;
  public search_by;
  public search_by1;
  public contains;
  public term;
  
  public valorSearch = "";
  public fq:any = "";
  public termino:any = "";
  public operador:any = "";
  public filtros:any = "";
  public filtro: any ="";
  public str_filtros:any = "";
  public urlFiltro;
  public formfiltro;

  public registros;
  public totalPage;
  public page;
  public npage;
  public cantidadReg;
  public paginacion= [];
  public nextPage;
  public prevPage;

  searchForm: FormGroup;

  constructor(
    private activatedRoute: ActivatedRoute, 
    private queriesService: QueriesService, 
    private formBuilder: FormBuilder, 
    private router: Router
    ) {
    var indexfiltro=0;
    this.createForm(indexfiltro);
    this.urlPhp = global.php;
  }

  ngOnInit() {
    if(this.data['append_filter'] == undefined) {
      this.data['append_filter'] = this.json['horizon_search_form']['filters']['content'][0];
    }
    this.data['title'] = 'Búsqueda avanzada';
    // Recibo datos por get para pasarlos a funcion searchPage
    this.activatedRoute.params.subscribe(params =>{
      this.data['param'] = params['search-term'];
      this.npage = parseInt(params['n-page']);
      this.searchPage(this.data['param'],this.npage);
    });
  }

  
  //Recibe de Búsqueda Simple y Avanzada, trae variable por url
  searchPage(param, npage){
    // JQuery ir arriba
    $('body, html').animate({
			scrollTop: '600px'
    }, 300);
    
    if(param=="home"){
      param = JSON.stringify({"search_form":"","search_by":"","contains":"","term":""});
    }
    this.objFiltro = JSON.parse(param);
    this.str_filtros = "";
    this.valorSearch = this.objFiltro['search_form'];
    //console.log(Object.keys(this.objFiltro));
    // Creo variables en localStorage
    //localStorage.setItem('jsonFiltro', param);
    //localStorage.setItem('search_form', this.objFiltro ['search_form']);
    //localStorage.removeItem('prueba');
    
    // Asigno variables de localStorage
    //this.jsonFiltro = localStorage.getItem('jsonFiltro');
    //this.objFiltro = JSON.parse(this.jsonFiltro);
    //this.valorSearch = localStorage.getItem('search_form');

    this.data['title'] = 'Resultados de búsqueda';
    npage = (npage-1)*20;

    var j = (Object.keys(this.objFiltro).length-1)/3;
    var pos = 0;
      console.log(this.objFiltro);
      //console.log(j);
    for(let i=0; i < j; i++){
        pos++;
        this.fq = Object.values(this.objFiltro)[pos];
     
        pos++;
        this.operador = Object.values(this.objFiltro)[pos];
      
        pos++;
        this.termino = Object.values(this.objFiltro)[pos];
      
      
        switch (this.fq) {
          case 'titulo':
            if(this.operador == 'no-contiene'){
              this.filtro = "fq=-title:"+this.termino+"%26";
            }else{
              this.filtro = "fq=title:"+this.termino+"%26";
            }
          break;
          case 'autor':
              if(this.operador == 'no-contiene'){
                this.filtro = "fq=-author:"+this.termino+"%26";
              }else{
                this.filtro = "fq=author:"+this.termino+"%26";
              }
          break;
          case 'fecha':
              if(this.operador == 'no-contiene'){
                this.filtro = "fq=-dateIssued:"+this.termino+"%26";
              }else{
                this.filtro = "fq=dateIssued:"+this.termino+"%26";
              }
          break;
          case 'materia':
              if(this.operador == 'no-contiene'){
                this.filtro = "fq=-subject:"+this.termino+"%26";
              }else{
                this.filtro = "fq=subject:"+this.termino+"%26";
              }
          break;
        } 
        
        this.str_filtros=this.str_filtros + this.filtro;
      }
      
    this.urlFiltro = this.urlPhp + 'filtro=' + this.str_filtros +  '&valor=' + this.valorSearch + '&start=' + npage;
    console.log(this.urlFiltro );

    this.queriesService.queryGet( this.urlFiltro )
      .then((data) => { 
        this.data['search']= Object.keys(data) .map(i =>data[i]);

        // filtros materia
        this.data['materia'] = this.data['search'][2]['facet_fields']['bi_4_dis_value_filter'];
        // filtros autores
        this.data['autores'] = this.data['search'][2]['facet_fields']['bi_2_dis_value_filter'];
        // filtros tipo documento
        this.data['tipodoc'] = this.data['search'][2]['facet_fields']['type_filter'];
        
        // PAGINACIÓN
        this.totalPage = this.data['search'][1]['numFound'];
        this.cantidadReg = 20;
        this.page  = Math.ceil(this.totalPage / this.cantidadReg); 
        
        console.log(this.data['search']);
        console.log(this.totalPage + " registros");
        console.log(this.page + " páginas" );
        console.log('pagina cada '+npage);
        console.log('página atual: '+this.npage);
        this.paginacion = []; // Dejar vacío para volver a crear loop con cada consulta
        for(let i=1; i<=this.page; i++){
          if(i <= 5){
            if(this.npage>5){
              this.paginacion.push(i+(this.npage-5));
            }else{
              this.paginacion.push(i);
            }
          }
        }
        if(this.npage>=2){
          this.prevPage = this.npage-1;
        }else{
          this.prevPage = 1;
        }
        if(this.npage<this.page){
          this.nextPage = this.npage+1;
        }else{
          this.nextPage = this.page;
        }
        // Fin paginación
        
        // MUESTRA/OCULTA SECCIÓN REGISTROS
        if(this.data['search']==""){
          this.registros=false;
        }else{
          this.registros=true;
        }
    });

  }

  getSearch(form, $event) {
    $event.preventDefault();
    if(form.value) {
      this.filtros = JSON.stringify(form.value);
      this.router.navigate(['/search-advanced/' + this.filtros + '/1/']);
    }
  }
/*   gettipoDoc(tipo){
    this.router.navigate(['/search-advanced/%7B"search_form":"'+this.valorSearch+'","search_by0":"titulo","contains0":"contiene","term0":"'+tipo+'"%7D/1']);
  } */
  addFilter(index) {

    this.json['horizon_search_form']['filters']['content'].push(this.data['append_filter']);
    this.createForm(index);
    
  }

  removeFilter(index) {
    this.json['horizon_search_form']['filters']['content'].splice(index, 1);
    this.searchForm.controls['search_by'+index].setValue("");
    this.searchForm.controls['contains'+index].setValue("");
    this.searchForm.controls['term'+index].setValue("");
  }

  createForm(index) {
    var index = index+1;

    this.searchForm = this.formBuilder.group({
      search_form: (''),

      search_by0: (''),
      contains0: (''),
      term0: (''),
      search_by1: (''),
      contains1: (''),
      term1: (''),
      search_by2: (''),
      contains2: (''),
      term2: (''),
      search_by3: (''),
      contains3: (''),
      term3: (''),
      search_by4: (''),
      contains4: (''),
      term4: (''),
      search_by5: (''),
      contains5: (''),
      term5: (''),
      search_by6: (''),
      contains6: (''),
      term6: (''),
      search_by7: (''),
      contains7: (''),
      term7: (''),
      search_by8: (''),
      contains8: (''),
      term8: (''),
      search_by9: (''),
      contains9: (''),
      term9: (''),
      search_by10: (''),
      contains10: (''),
      term10: ('')

      /* ssearch_form: ['', Validators.required ],

      search_by0: ['', Validators.required ],
      contains0: ['', Validators.required ],
      term0: ['', Validators.required ],

      search_by1: ['', Validators.required ],
      contains1: ['', Validators.required ],
      term1: ['', Validators.required ]

      earch_by2: ['', Validators.required ],
      contains2: ['', Validators.required ],
      term2: ['', Validators.required ],

      search_by3: ['', Validators.required ],
      contains3: ['', Validators.required ],
      term3: ['', Validators.required ],

      search_by4: ['', Validators.required ],
      contains4: ['', Validators.required ],
      term4: ['', Validators.required ],

      search_by5: ['', Validators.required ],
      contains5: ['', Validators.required ],
      term5: ['', Validators.required ],

      search_by6: ['', Validators.required ],
      contains6: ['', Validators.required ],
      term6: ['', Validators.required ],

      search_by7: ['', Validators.required ],
      contains7: ['', Validators.required ],
      term7: ['', Validators.required ],

      search_by8: ['', Validators.required ],
      contains8: ['', Validators.required ],
      term8: ['', Validators.required ],

      search_by9: ['', Validators.required ],
      contains9: ['', Validators.required ],
      term9: ['', Validators.required ],

      search_by10: ['', Validators.required ],
      contains10: ['', Validators.required ],
      term10: ['', Validators.required ] */
    });
  }

}
