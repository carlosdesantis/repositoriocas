import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params }     from '@angular/router';
import { Router }                   from '@angular/router';
import { QueriesService }    from '../../services/queries.service';
import { global } from '../../services/global';
import json                  from './single-record.json';

@Component({
  selector: 'app-single-record',
  templateUrl: './single-record.component.html',
  styleUrls: ['./single-record.component.css']
})
export class SingleRecordComponent implements OnInit {

  data: any = [];
  json: any = json;
  public urlPhp;
  metadatos: any = [];

  public_id: any = '1000';

  response: any = {
    'show': false,
    'data': ''
  };
  liked: any = false;

  constructor(
    private activatedRoute: ActivatedRoute, 
    private router: Router,
    private queriesService: QueriesService
    ) { 
      this.urlPhp = global.php_ficha;
    }

  ngOnInit() {



    // recibe valor handle de url
    this.activatedRoute.params.subscribe(params =>{
      this.data['param1'] = params['param1'];
      this.data['param2'] = params['param2'];
      this.data['param'] = this.data['param1'] + "/" + this.data['param2']
      this.queriesService.queryGet(this.urlPhp +'handle=' + this.data['param']).then((data) => {
      console.log(data);

      let list = data['response']['docs'][0];
      for(let i in list){
        if (i.substr(0,3)  == 'dc.'){
          this.metadatos.push( { "metadato": i, "valor": list[i] })
        }
      }


      this.data['publics'] = data['response']['docs'][0];
      });

    });

    this.queriesService.queryGet('http://localhost:3000/publics').then((data) => {
      this.data['publicaciones'] = data});
    this.queriesService.queryGet('http://localhost:3000/comments').then((data) => {
      this.data['comments'] = data});

    this.likeRecord();
  }

  likeRecord() {
    this.queriesService.queryGet('http://localhost:3000/likes').then((data) => {
      this.liked = data['liked'];
      this.queriesService.queryPost('http://localhost:3000/likes', {liked: !data['liked']});
    });
  }

  resetForm() {
    this.response.show = false;
    this.response.text = '';
  }

  submitComment(form, $event) {
    $event.preventDefault();
    if(form.form.value) {
      let date = new Date();
      let full_date = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes();
      let comment = {
        user: "Napoleón Gómez", //Reemplazar por el usuario actual que haya iniciado sesión
        date: full_date,
        text: form.form.value['comment_textarea']
      }
      this.queriesService.queryPost('http://localhost:3000/comments', comment).then(
        (data) => {
          this.queriesService.queryGet('http://localhost:3000/comments').then((data) => {this.data['comments'] = data});
          this.response.show = true;
          this.response.text = 'Su comentario ha sido publicado con éxito';
        },
        (error) => {
          this.response.show = true;
          this.response.text = 'Ha ocurrido un problema. Por favor, intente más tarde';
        }
      );
    }
  }

  descargaGA(){
    console.log(this.data['publics']['title'][0]);
    this.data['tit'] = this.data['publics']['title'][0];
    this.router.navigate(['/descarga-ficha/'+this.data['tit']]);
  }


}
