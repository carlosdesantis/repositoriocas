import { Component, OnInit } from '@angular/core';
import { QueriesService }    from '../../services/queries.service';
import json                  from './profile-team.json';

@Component({
  selector: 'app-profile-team',
  templateUrl: './profile-team.component.html',
  styleUrls: ['./profile-team.component.css']
})
export class ProfileTeamComponent implements OnInit {

  data: any = [];
  json: any = json;

  constructor(private queriesService: QueriesService) { }

  ngOnInit() {
    this.queriesService.queryGet('http://localhost:3000/users?id=00000001').then((data) => {
      this.data['user'] = data[0];
    });
    this.queriesService.queryGet('http://localhost:3000/users').then((data) => {this.data['users'] = data});

    // JQuery ir arriba
    $('body, html').animate({
      scrollTop: '0px'
    }, 300);

  }


  printProfile(dataPrint) {
    let printContents = dataPrint.innerHTML;
    let originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
  }

}
