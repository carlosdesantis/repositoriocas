import { Component, OnInit }  from '@angular/core';
import { QueriesService }     from '../../services/queries.service';
import json                   from '../../../assets/json/bibliometrics.json';
import Studies from '../../../assets/json/studies.json';

@Component({
  selector: 'app-bibliometrics',
  templateUrl: './bibliometrics.component.html',
  styleUrls: ['./bibliometrics.component.css']
})
export class BibliometricsComponent implements OnInit {

  data:               any = [];
  mainBibliometrics:  any = {};

  constructor(private queriesService: QueriesService) { }

  ngOnInit() {
    this.mainBibliometrics = json;
    //this.queriesService.queryGet('http://localhost:3000/studies').then( (data) => {this.data['studies'] = data;});
    this.data['studies'] = Studies;
  }

  scrollTo(element, $event) {
    $event.preventDefault();
    document.getElementById(element).scrollIntoView({behavior: 'smooth'});
  }
}
