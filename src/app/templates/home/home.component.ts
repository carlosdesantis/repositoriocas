import { Component, OnInit }                      from '@angular/core';
import { Observable }                             from 'rxjs';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { Chart }                                  from 'chart.js';

import { QueriesService }                         from '../../services/queries.service';
import json                                       from './home.json';
import AreasTematicas from '../../../assets/json/areas-tematicas.json';
import CajonBusqueda from '../../../assets/json/cajon-busqueda.json';
import EstudioBibliometricos from '../../../assets/json/estudios-bibliometricos.json';

import { FormGroup,  FormBuilder,  Validators }   from '@angular/forms';
import { Router }                   from '@angular/router';

import { ApiService } from '../../services/api.service';
import { NgForm } from '@angular/forms';
import { SubscribeService } from '../../services/subscribe.service';




import { environment } from "../../../environments/environment";
import 'rxjs/add/operator/finally';
import 'rxjs/add/operator/mergeMap';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ApiService]
})
export class HomeComponent implements OnInit {

  json:     any = json;
  posts:    any = AreasTematicas['areas-tematicas'];
  CajonBusqueda: any = CajonBusqueda;
  data:     any = [];
  loggedIn: any = false;
  editing:  any = false;
  userData: any = [];
  loginFailed: any = "";

  angForm: FormGroup;
  editForm: FormGroup;

  status;
  
  public buscalibro = { name: ''};

  
  subscribeData: any = <any>{};
  

/*   title = 'ngSlick';
 
 
  slides = [
    {img: "../assets/img/2.gif"},
    {img: "../assets/img/bnr-libros.jpg"},
    {img: "../assets/img/ciencias-computacion.jpg"},
    {img: "../assets/img/ciencias-computacion1.jpg"},
    {img: "../assets/img/ciencias-computacion2.jpg"},
    {img: "../assets/img/ciencias-sociales.jpg"},
    {img: "../assets/img/filosofia.jpg"},
    {img: "../assets/img/psicologia.jpg"},
    {img: "../assets/img/psicologia2.jpg"},
    {img: "../assets/img/religion.jpg"}
  ];
 
  slideConfig = {
    "slidesToShow": 4, 
    "slidesToScroll": 1,
    "nextArrow":"<div class='nav-btn next-slide'></div>",
    "prevArrow":"<div class='nav-btn prev-slide'></div>",
    "dots":true,
    "infinite": false
  }; */

  

  constructor(
    private _ApiService: ApiService,
    private subscribeService: SubscribeService,
    private router: Router,
    private queriesService: QueriesService, private _sanitizer: DomSanitizer, private formBuilder: FormBuilder
    ) 
    {
    this.createForm();
    }

  ngOnInit() {
    
    // TEXTOS PÁGINA
    this.json = json;
    
    // BÚSQUEDA
    //this.getCajonBusqueda();
    /* this.queriesService.queryGet('http://146.155.154.168/rest/public/api/busqueda?valor=VIOLETA').then((data) => {
      this.data['search'] = data;
    }); */

    // AREAS TEMÁTICAS
    this.data['topics'] = this.posts;
    //this.getTematicas();
    /*this.queriesService.queryGet('http://localhost:3000/topics').then((data) => {
      this.data['topics'] = data;
    });*/

    /* this.queriesService.queryGet('http://146.155.154.168/rest/public/api/suscripcion-news').then((data) => {
      this.data['SuscripcionNews'] = data;
    }); */
    this.data['SuscripcionNews'] = json['horizon_novedades']['section_suscription'];

    /* this.queriesService.queryGet('http://146.155.154.168/rest/public/api/compartir-investigacion').then((data) => {
      this.data['CompartirInvestigacion'] = data;
    }); */

    this.queriesService.queryGet('http://localhost:3000/news').then((data) => {
      this.data['news'] = data
    });

    //this.queriesService.queryGet('http://localhost:3000/statistics').then((data) => {
    this.queriesService.queryGet('http://interfaz-rep-desa-lab.dca.uc.cl/assets/php/cantidad-tesis.php').then((data) => {
      this.data['statistics-tesis'] = data['response']['numFound'];
      //this.CreateGraphs();
    });
    this.queriesService.queryGet('http://interfaz-rep-desa-lab.dca.uc.cl/assets/php/cantidad-articulos.php').then((data) => {
      this.data['statistics-articulos'] = data['response']['numFound'];
    });

    /* this.queriesService.queryGet('http://localhost:3000/studies').then((data) => {
      this.data['studies'] = data
    }); */
    this.data['studies'] = EstudioBibliometricos['estudios-bibliometricos'];

    if(screen.width < 1500){
      $('#containerBusca').addClass('margin-top-300');
      $('#cajaSearch').addClass('fixed');
      this.fixedCaja();
    }
    
  }

  fixedCaja(){
    //$('#containerBusca').css('padding-top','200px');

    $(window).scroll(function() {    
      if ($(window).scrollTop() == 0){
        $('#cajaSearch').addClass('fixed');
        $('#containerBusca').addClass('margin-top-300');
    } else {
        $('#cajaSearch').removeClass('fixed');
        $('#containerBusca').removeClass('margin-top-300');
        //$('#containerBusca').css('margin-top','0px');
    }
    });

  }

  subscribe(form) {

    this.subscribeService.subscribeToList(this.subscribeData)
      .subscribe(res => {
        this.status = "success";
      }, err => {
        console.log(err);
      })
  }


  llamarLogin() {

    window.location.href = `${environment.cas_login_url}?service=${encodeURIComponent(window.location.href)}`;
 
  }

/*   getTematicas(){
    this._ApiService.getTematicas().subscribe(
      response => {
        
          this.posts = response;
          this.posts = Object.keys(this.posts) .map(i =>this.posts [i]);   
      },
      error => {
        console.log(error);
      }
    );
  } */

  getSearch(form) {
    var filtro = '{"search_form":"'+ this.buscalibro.name +'","search_by0":"","contains0":"","term0":""}';
    this.router.navigate(['/search-advanced/'+ filtro + '/1']);
  }
 
/*   getCajonBusqueda(){
    this._ApiService.getCajonBusqueda().subscribe(
      response => {
          this.CajonBusqueda = response;
          this.CajonBusqueda = Object.keys(this.CajonBusqueda) .map(i =>this.CajonBusqueda [i]);    
      },
      error => {
        console.log(error);
      }
    );
  } */

  CreateGraphs(){
    if(this.data['statistics']){
      this.data['statistics']['graphics'].forEach(function(graphic, key){
        setTimeout(function(){
          let myChart = new Chart((<HTMLCanvasElement>document.getElementById('chart-'+key)).getContext('2d'), graphic);
        }, 50)
      });
    }
  }

  logIn(obj, $event){
    $event.preventDefault();
    if(obj.value){
      this.queriesService.queryGet('http://localhost:3000/login?email=' + obj.value['email'] + '&password=' + obj.value['password']).then((data: any[]) => {
        if(data.length > 0) {
          this.loggedIn = true;
          this.userData = data[0];
        } else {
          this.loginFailed = 'Usuario o contraseña incorrecto.';
        }
      });
    }
  }

  editData(obj, $event){
    $event.preventDefault();
    if(!this.editing){
      this.editing = true;
      this.editForm.setValue({
        name: this.userData.name,
        email: this.userData.email,
        email_privacy: this.userData.email_privacy,
        phone: this.userData.phone,
        phone_privacy: this.userData.phone_privacy,
        unity: this.userData.unity,
        newsletter: this.userData.newsletter
      })
    } else {
      this.queriesService.queryPut('http://localhost:3000/login/' + this.userData['id'], obj.value).then((data) => {
        this.userData = data;
        this.editing = false;
      });
    }
  }

  getBackgroundImage(image) {
    return this._sanitizer.bypassSecurityTrustStyle(`linear-gradient(rgba(29, 29, 29, 0), rgba(16, 16, 23, 0.5)), url(${image})`);
  }

  createForm() {
    this.angForm = this.formBuilder.group({
      email: ['', Validators.required ],
      password: ['', Validators.required ]
    });

    this.editForm = this.formBuilder.group({
      name: ['', Validators.required ],
      email: ['', Validators.required ],
      email_privacy: ['', Validators.required ],
      phone: ['', Validators.required ],
      phone_privacy: ['', Validators.required ],
      unity: ['', Validators.required ],
      newsletter: ['', Validators.required ],
    });
  }



/* 
  addSlide() {
    this.slides.push({img: "http://placehold.it/350x150/777777"})
  }
  
  removeSlide() {
    this.slides.length = this.slides.length - 1;
  }
  
  slickInit(e) {
    console.log('slick initialized');
  }
  
  breakpoint(e) {
    console.log('breakpoint');
  }
  
  afterChange(e) {
    console.log('afterChange');
  }
  
  beforeChange(e) {
    console.log('beforeChange');
  } */
  

}
