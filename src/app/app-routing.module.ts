import { NgModule }                   from '@angular/core';
import { Routes, RouterModule }       from '@angular/router';

import { AppComponent }               from './app.component';
import { HomeComponent }              from './templates/home/home.component';
import { BibliometricsComponent }     from './templates/bibliometrics/bibliometrics.component';
import { ContactComponent }           from './templates/contact/contact.component';
import { CoverContentComponent }      from './templates/cover-content/cover-content.component';
import { CoverFirstLevelComponent }   from './templates/cover-first-level/cover-first-level.component';
import { CoverNoveltyComponent }      from './templates/cover-novelty/cover-novelty.component';
import { CoverSecondLevelComponent }  from './templates/cover-second-level/cover-second-level.component';
import { FaqComponent }               from './templates/faq/faq.component';
import { PageContentComponent }       from './templates/page-content/page-content.component';
import { ProfileResearcherComponent } from './templates/profile-researcher/profile-researcher.component';
import { ProfileTeamComponent }       from './templates/profile-team/profile-team.component';
import { SearchAdvancedComponent }    from './templates/search-advanced/search-advanced.component';
import { SingleNoveltyComponent }     from './templates/single-novelty/single-novelty.component';
import { SingleRecordComponent }      from './templates/single-record/single-record.component';
import { UploadRecordComponent }      from './templates/upload-record/upload-record.component';
import { DescargaGaComponent }      from './templates/descarga-ga/descarga-ga.component';

const routes: Routes = [
  { path: '',                   component: HomeComponent },
  { path: 'bibliometrics',      component: BibliometricsComponent },
  { path: 'contact',            component: ContactComponent },
  { path: 'cover-content',      component: CoverContentComponent },
  { path: 'cover-first-level',  component: CoverFirstLevelComponent },
  { path: 'cover-novelty',      component: CoverNoveltyComponent },
  { path: 'cover-second-level', component: CoverSecondLevelComponent },

  { path: 'tesis',                    component: CoverFirstLevelComponent },
  { path: 'tesis-doctorado-magister', component: CoverSecondLevelComponent },
  { path: 'trabajos-graduacion',      component: CoverSecondLevelComponent },
  { path: 'subir-tesis-trabajos',     component: UploadRecordComponent },

  { path: 'publicaciones',            component: CoverFirstLevelComponent },
  { path: 'articulos-academicos',     component: CoverSecondLevelComponent },
  { path: 'capitulos-libros',         component: CoverSecondLevelComponent },
  { path: 'libros',                   component: CoverSecondLevelComponent },
  { path: 'documentos-trabajo',       component: CoverSecondLevelComponent },
  { path: 'presentaciones-congresos', component: CoverSecondLevelComponent },
  { path: 'obras-artisticas',         component: CoverSecondLevelComponent },
  { path: 'otros',                    component: CoverSecondLevelComponent },
  { path: 'subir-publicacion',        component: UploadRecordComponent },

  { path: 'enlaces-recursos',         component: CoverFirstLevelComponent },
  { path: 'otros-repositorios',       component: CoverContentComponent },
  { path: 'recursos-uc',              component: CoverContentComponent },
  { path: 'patentes-uc',              component: CoverContentComponent },

  { path: 'novedades',                component: CoverNoveltyComponent },
  { path: 'subidos-recientemente',    component: PageContentComponent },
  { path: 'mas-descargados',          component: PageContentComponent },
  { path: 'destacados',               component: PageContentComponent },

  { path: 'faq',                component: FaqComponent },
  { path: 'page-content',       component: PageContentComponent },
  { path: 'profile-researcher', component: ProfileResearcherComponent },
  { path: 'profile-team',       component: ProfileTeamComponent },
  { path: 'search-advanced',    component: SearchAdvancedComponent },
  { path: 'single-novelty',     component: SingleNoveltyComponent },
  { path: 'single-record',      component: SingleRecordComponent },
  { path: 'upload-record',      component: UploadRecordComponent },


  /* Ruta con parámetros */
  { path: 'search-advanced/:search-term/:n-page/:search_by/:contains/:tituloAutor', component: SearchAdvancedComponent },
  { path: 'search-advanced/:search-term/:n-page', component: SearchAdvancedComponent },
  { path: 'faq/:search-term',                     component: FaqComponent },
  { path: 'portada/:pag',                         component: CoverFirstLevelComponent },
  { path: 'pagina/:pag',                          component: CoverSecondLevelComponent },
  { path: 'upload/:pag',                          component: UploadRecordComponent },
  { path: 'enlaces/:pag',                         component: CoverContentComponent },
  { path: 'news/:pag',                            component: CoverNoveltyComponent },
  { path: 'content/:pag',                         component: PageContentComponent },
  { path: 'handle/:param1/:param2',               component: SingleRecordComponent },
  { path: 'descarga-ficha/:titulo',               component: DescargaGaComponent },

  /* Ruta wildcard, si la ruta no existe, redirige al Home */
  { path: '**',                 redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
