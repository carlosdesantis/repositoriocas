import { Component, OnInit }  from '@angular/core';
import json                   from './footer.json';
//import footer                 from './footer-01.json';
import footer                 from '../../../assets/json/footer-01.json';
import MenuRepos                 from '../../../assets/json/menu-repositorio.json';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  //json:       any = json;
  footer:     any = footer['footer-01'];
  menuRepos:  any = MenuRepos['menu-repositorio-uc'];

  constructor() { }

  ngOnInit() {
  }

}
