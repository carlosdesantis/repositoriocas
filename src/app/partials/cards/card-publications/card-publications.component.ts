import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-publications',
  templateUrl: './card-publications.component.html',
  styleUrls: ['./card-publications.component.css']
})
export class CardPublicationsComponent implements OnInit {

  @Input() data:        any;
  @Input() heightSame:  any;

  constructor() { }

  ngOnInit() {
  }

}
