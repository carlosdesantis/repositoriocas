import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-topic',
  templateUrl: './card-topic.component.html',
  styleUrls: ['./card-topic.component.css']
})
export class CardTopicComponent implements OnInit {

  @Input() cardTopicData: any;
  @Input() cardTopicIndex: any;
  @Input() cardTopicImage: any;

  constructor() { }

  ngOnInit() {
  }

}
