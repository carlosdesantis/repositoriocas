import { Component, OnInit, Input } from '@angular/core';
import { Router }                   from '@angular/router';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css']
})
export class SearchFormComponent implements OnInit {

  @Input() data: any;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  getSearch(form, $event) {
    $event.preventDefault();
    if(form.form.value) {
      this.router.navigateByUrl('/search-advanced/'+form.form.value.ucsearch);
    }
  }
}
